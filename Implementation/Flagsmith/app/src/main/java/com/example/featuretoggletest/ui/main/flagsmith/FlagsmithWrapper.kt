package com.example.featuretoggletest.ui.main.flagsmith

import com.solidstategroup.bullettrain.BulletTrainClient
import com.solidstategroup.bullettrain.FeatureUser
import com.solidstategroup.bullettrain.Flag

object FlagsmithWrapper {

    private var flags = listOf<Flag>()
    private var user: FeatureUser? = null

    private var flagsmith = BulletTrainClient.newBuilder()
        .setApiKey("Btc6iUhNUa9zZPbyggeMcd")
        .build()

    fun setUser(user: FeatureUser) {
        this.user = user
    }

    fun fetchAsync() {
        Thread {
            flags = flagsmith.getFeatureFlags(user)
        }.start()
    }

    fun hasFeatureFlag(featureId: String): Boolean {
        return flags.any {
            it.feature.name == featureId && it.isEnabled
        }
    }

    fun getFlagValue(featureId: String): String {
        return flags.first {
            it.feature.name == featureId
        }.stateValue
    }


    fun getFlagValueInt(featureId: String): Int {
        val value = getFlagValue(featureId)
        return value.toInt()
    }

    fun getFlagValueDouble(featureId: String): Double {
        val value = getFlagValue(featureId)
        return value.toDouble()
    }

}