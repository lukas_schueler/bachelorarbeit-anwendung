package com.example.featuretoggletest.ui.main

import android.content.res.Resources
import android.graphics.Color
import android.icu.text.DecimalFormat
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.featuretoggletest.R
import com.example.featuretoggletest.databinding.FragmentShoppingCartBinding
import com.example.featuretoggletest.ui.main.flagsmith.FlagsmithWrapper

class ShoppingCartFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: FragmentShoppingCartBinding

    private var flagsmith = FlagsmithWrapper

    private var shippingCost = flagsmith.getFlagValueDouble("shipping_cost")
    private var discountPercent = flagsmith.getFlagValueInt("discount_10_euro")

    companion object {
        fun newInstance() = ShoppingCartFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShoppingCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        setupTextViews()
        setupButton()
    }

    private fun setupTextViews() {
        binding.articleCount.text = viewModel.getShoppingCartItemCount().toString()
        binding.price.text = "${viewModel.getShoppingCartSum()} €"
        binding.shippingPrice.text = "${shippingCost} €"
        binding.discountAmount.text = "${discountPercent}%"

        val totalAmount = gerTotalAmount()
        val df = DecimalFormat("#.##")

        binding.totalAmount.text = "${df.format(totalAmount)} €"
    }

    private fun gerTotalAmount(): Double {
        val discountMultiplikator = (100f - discountPercent.toFloat()) / 100f
        return viewModel.getShoppingCartSum() * discountMultiplikator + shippingCost
    }

    private fun setupButton() {
        if(viewModel.getShoppingCartItemCount() == 0) {
            binding.buyButton.isEnabled = false
            binding.buyButton.setBackgroundColor(context!!.getColor(R.color.grey))
        } else {
            val colorString = flagsmith.getFlagValue("buy_button_color")
            binding.buyButton.setBackgroundColor(Color.parseColor(colorString))
        }

        binding.buyButton.setOnClickListener {
            viewModel.isItem1InCart = false
            viewModel.isItem2InCart = false
            viewModel.isItem3InCart = false

            requireActivity().supportFragmentManager.popBackStack()
        }
    }
}