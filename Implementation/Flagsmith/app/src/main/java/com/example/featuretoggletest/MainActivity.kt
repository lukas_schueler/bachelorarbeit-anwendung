package com.example.featuretoggletest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.*
import com.example.featuretoggletest.databinding.MainActivityBinding
import com.example.featuretoggletest.ui.main.MainFragment
import com.example.featuretoggletest.ui.main.ShoppingCartFragment
import com.example.featuretoggletest.ui.main.flagsmith.FlagsmithWrapper
import com.solidstategroup.bullettrain.FeatureUser
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private var flagsmith = FlagsmithWrapper

    private var isNewLoadingAnimationEnabled = false

    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val user = FeatureUser()
        user.identifier = "first_sample_user"
        flagsmith.setUser(user)
        flagsmith.fetchAsync()

        if (savedInstanceState == null) {
            Thread {
                Thread.sleep(2000)
                this.runOnUiThread {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, MainFragment.newInstance(getActivityListener()))
                        .commitNow()
                }
            }.start()
        }
    }

    override fun onResume() {
        super.onResume()
        Thread {
            try {
                isNewLoadingAnimationEnabled = flagsmith.hasFeatureFlag("new_loading_animation")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }.start()

        if (isNewLoadingAnimationEnabled) {
            showCompleteOverlay()
            startLoadingAnimation()
        } else {
            removeCompleteOverlay()
        }
    }

    private fun startLoadingAnimation() {
        scaleImageUp { scaleImageDown { scaleImageUp { scaleImageDown { scaleImageUp {  endLoadingAnimation() } } } } }
    }

    private fun scaleImageUp(endAction: () -> Unit) {
        binding.loadingImage.animate()
            .setStartDelay(500)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .setDuration(1000)
            .scaleXBy(1.5f)
            .scaleYBy(1.5f)
            .withEndAction(endAction)
            .start()
    }

    private fun scaleImageDown(endAction: () -> Unit) {
        binding.loadingImage.animate()
            .setStartDelay(500)
            .setInterpolator(BounceInterpolator())
            .setDuration(1000)
            .scaleXBy(-1.5f)
            .scaleYBy(-1.5f)
            .withEndAction(endAction)
            .start()
    }

    private fun endLoadingAnimation() {
        removeViewAnimated(binding.loadingImage)
        removeViewAnimated(binding.loadingTextview, 500)
        fadeoutView(binding.overlay, 1000) { removeCompleteOverlay() }
    }

    private fun removeViewAnimated(view: View, startDelay: Long = 0L) {
        view.animate()
            .setInterpolator(AnticipateInterpolator())
            .setStartDelay(startDelay)
            .setDuration(500)
            .scaleX(0f)
            .scaleY(0f)
            .start()
    }

    private fun fadeoutView(view: View, startDelay: Long, endAction: () -> Unit = {}) {
        view.animate()
            .setStartDelay(startDelay)
            .setDuration(500)
            .alpha(0f)
            .withEndAction(endAction)
            .start()
    }

    private fun removeCompleteOverlay() {
        setCompleteOverlayVisibility(View.GONE)
    }

    private fun showCompleteOverlay() {
        setCompleteOverlayVisibility(View.VISIBLE)
        binding.overlay.alpha = 1f
        binding.loadingTextview.scaleX = 1f
        binding.loadingTextview.scaleY = 1f
        binding.loadingImage.scaleX = 1f
        binding.loadingImage.scaleY = 1f

    }

    private fun setCompleteOverlayVisibility(visibility: Int) {
        binding.loadingImage.visibility = visibility
        binding.loadingTextview.visibility = visibility
        binding.overlay.visibility = visibility
    }

    private fun getActivityListener(): Listener {
        return object : Listener {
            override fun onShoppingCartClicked() {
                    supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                        .replace(R.id.container, ShoppingCartFragment.newInstance())
                        .addToBackStack(null)
                        .commit()
            }
        }
    }

    interface Listener {
        fun onShoppingCartClicked()
    }
}