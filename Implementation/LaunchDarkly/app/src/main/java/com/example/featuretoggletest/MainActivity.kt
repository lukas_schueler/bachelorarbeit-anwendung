package com.example.featuretoggletest

import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnticipateInterpolator
import android.view.animation.BounceInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.example.featuretoggletest.databinding.MainActivityBinding
import com.example.featuretoggletest.ui.main.MainFragment
import com.example.featuretoggletest.ui.main.ShoppingCartFragment
import com.launchdarkly.android.LDClient
import com.launchdarkly.android.LDConfig
import com.launchdarkly.android.LDUser


class MainActivity : AppCompatActivity() {

    private var isNewLoadingAnimationEnabled = true

    private lateinit var binding: MainActivityBinding
    private lateinit var ldConfig: LDConfig
    private lateinit var ldClient: LDClient
    private lateinit var user: LDUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getLaunchDarklyConfiguration()

        if (savedInstanceState == null) {
            Thread {
                Thread.sleep(2000)
                this.runOnUiThread {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, MainFragment.newInstance(getMainActivityListener()))
                        .commitNow()
                }
            }.start()
        }
    }

    override fun onResume() {
        super.onResume()

        isNewLoadingAnimationEnabled = ldClient.boolVariation("is_new_loading_animation_enabled", false)

        if (isNewLoadingAnimationEnabled) {
            showCompleteOverlay()
            startLoadingAnimation()
        } else {
            removeCompleteOverlay()
        }
    }

    private fun getLaunchDarklyConfiguration() {
        ldConfig = LDConfig.Builder()
            .setMobileKey("mob-dad0084f-2201-4194-aeb5-169c59929d7c")
            .build()

        user = LDUser.Builder("attribute user")
            .email("attribute@user.com")
            .build()

        ldClient = LDClient.init(application, ldConfig, user, 5)
    }

    private fun startLoadingAnimation() {
        scaleImageUp { scaleImageDown { scaleImageUp { scaleImageDown { scaleImageUp {  endLoadingAnimation() } } } } }
    }

    private fun scaleImageUp(endAction: () -> Unit) {
        binding.loadingImage.animate()
            .setStartDelay(500)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .setDuration(1000)
            .scaleXBy(1.5f)
            .scaleYBy(1.5f)
            .withEndAction(endAction)
            .start()
    }

    private fun scaleImageDown(endAction: () -> Unit) {
        binding.loadingImage.animate()
            .setStartDelay(500)
            .setInterpolator(BounceInterpolator())
            .setDuration(1000)
            .scaleXBy(-1.5f)
            .scaleYBy(-1.5f)
            .withEndAction(endAction)
            .start()
    }

    private fun endLoadingAnimation() {
        removeViewAnimated(binding.loadingImage)
        removeViewAnimated(binding.loadingTextview, 500)
        fadeoutView(binding.overlay, 1000) { removeCompleteOverlay() }
    }

    private fun removeViewAnimated(view: View, startDelay: Long = 0L) {
        view.animate()
            .setInterpolator(AnticipateInterpolator())
            .setStartDelay(startDelay)
            .setDuration(500)
            .scaleX(0f)
            .scaleY(0f)
            .start()
    }

    private fun fadeoutView(view: View, startDelay: Long, endAction: () -> Unit = {}) {
        view.animate()
            .setStartDelay(startDelay)
            .setDuration(500)
            .alpha(0f)
            .withEndAction(endAction)
            .start()
    }

    private fun removeCompleteOverlay() {
        setCompleteOverlayVisibility(View.GONE)
    }

    private fun showCompleteOverlay() {
        setCompleteOverlayVisibility(View.VISIBLE)
        binding.overlay.alpha = 1f
        binding.loadingTextview.scaleX = 1f
        binding.loadingTextview.scaleY = 1f
        binding.loadingImage.scaleX = 1f
        binding.loadingImage.scaleY = 1f

    }

    private fun setCompleteOverlayVisibility(visibility: Int) {
        binding.loadingImage.visibility = visibility
        binding.loadingTextview.visibility = visibility
        binding.overlay.visibility = visibility
    }

    private fun getMainActivityListener(): MainListener {
        return object : MainListener {
            override fun onShoppingCartClicked() {
                    supportFragmentManager.beginTransaction()
                        .setCustomAnimations(
                            R.anim.enter_from_right,
                            R.anim.exit_to_left,
                            R.anim.enter_from_left,
                            R.anim.exit_to_right
                        )
                        .replace(R.id.container, ShoppingCartFragment.newInstance(getShoppingCartListener()))
                        .addToBackStack(null)
                        .commit()
            }
        }
    }

    private fun getShoppingCartListener(): ShoppingCartListener {
        return object : ShoppingCartListener {
            override fun onBuyButtonClicked(sum: Double) {
                user = LDUser.Builder(user)
                    .custom("last_shopping_cart_sum", sum)
                    .build()

                ldClient.identify(user)
            }
        }
    }

    interface MainListener {
        fun onShoppingCartClicked()
    }

    interface ShoppingCartListener {
        fun onBuyButtonClicked(sum: Double)
    }
}