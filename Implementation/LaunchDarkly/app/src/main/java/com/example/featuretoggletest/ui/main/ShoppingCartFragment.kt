package com.example.featuretoggletest.ui.main

import android.graphics.Color
import android.icu.text.DecimalFormat
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.featuretoggletest.MainActivity
import com.example.featuretoggletest.R
import com.example.featuretoggletest.databinding.FragmentShoppingCartBinding
import com.launchdarkly.android.LDClient

class ShoppingCartFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: FragmentShoppingCartBinding
    private lateinit var activityListener: MainActivity.ShoppingCartListener

    private var shippingCost = LDClient.get().doubleVariation("shipping_cost", 4.99)
    private var discountPercent = LDClient.get().doubleVariation("shopping_cart_discount", 0.0)

    companion object {
        fun newInstance(listener: MainActivity.ShoppingCartListener): ShoppingCartFragment {
            return ShoppingCartFragment().apply {
                activityListener = listener
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShoppingCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        setupTextViews()
        setupButton()
    }

    private fun setupTextViews() {
        binding.articleCount.text = viewModel.getShoppingCartItemCount().toString()
        binding.price.text = "${viewModel.getShoppingCartSum()} €"
        binding.shippingPrice.text = "${shippingCost} €"
        binding.discountAmount.text = "${discountPercent}%"

        val totalAmount = gerTotalAmount()
        val df = DecimalFormat("#.##")

        binding.totalAmount.text = "${df.format(totalAmount)} €"
    }

    private fun gerTotalAmount(): Double {
        val discountMultiplikator = (100f - discountPercent.toFloat()) / 100f
        return viewModel.getShoppingCartSum() * discountMultiplikator + shippingCost
    }

    private fun setupButton() {
        if(viewModel.getShoppingCartItemCount() == 0) {
            binding.buyButton.isEnabled = false

            context?.getColor(R.color.grey)?.let {
                binding.buyButton.setBackgroundColor(it)
            }
        } else {
            val color = LDClient.get().stringVariation("buy_button_color", "#FF018786")
            binding.buyButton.setBackgroundColor(Color.parseColor(color))
        }

        binding.buyButton.setOnClickListener {
            activityListener.onBuyButtonClicked(gerTotalAmount())
            viewModel.isItem1InCart = false
            viewModel.isItem2InCart = false
            viewModel.isItem3InCart = false

            requireActivity().supportFragmentManager.popBackStack()
        }
    }
}