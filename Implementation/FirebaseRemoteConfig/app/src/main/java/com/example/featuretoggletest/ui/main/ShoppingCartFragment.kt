package com.example.featuretoggletest.ui.main

import android.graphics.Color
import android.icu.text.DecimalFormat
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.featuretoggletest.MainActivity
import com.example.featuretoggletest.R
import com.example.featuretoggletest.databinding.FragmentShoppingCartBinding
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig

class ShoppingCartFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: FragmentShoppingCartBinding
    private val remoteConfig = Firebase.remoteConfig

    private var shippingCost = remoteConfig.getDouble("shipping_cost")
    private var discountPercent = remoteConfig.getLong("discount_10_euro")

    private lateinit var listener: MainActivity.ShoppingCartListener

    companion object {
        fun newInstance(listener: MainActivity.ShoppingCartListener) : ShoppingCartFragment {
            return ShoppingCartFragment().apply {
                this.listener = listener
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShoppingCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        setupTextViews()
        setupButton()
    }

    private fun setupTextViews() {
        binding.articleCount.text = viewModel.getShoppingCartItemCount().toString()
        binding.price.text = "${viewModel.getShoppingCartSum()} €"
        binding.shippingPrice.text = "${shippingCost} €"
        binding.discountAmount.text = "${discountPercent}%"

        val totalAmount = getTotalAmount()
        val df = DecimalFormat("#.##")

        binding.totalAmount.text = "${df.format(totalAmount)} €"
    }

    private fun getTotalAmount(): Double {
        val discountMultiplikator = (100f - discountPercent.toFloat()) / 100f
        return viewModel.getShoppingCartSum() * discountMultiplikator + shippingCost
    }

    private fun setupButton() {
        if(viewModel.getShoppingCartItemCount() == 0) {
            binding.buyButton.isEnabled = false
            binding.buyButton.setBackgroundColor(context!!.getColor(R.color.grey))
        } else {
            val buttonColor = remoteConfig.getString("buy_button_color")
            context?.let{
                binding.buyButton.setBackgroundColor(Color.parseColor(buttonColor))
            }
        }

        binding.buyButton.setOnClickListener {
            val amount = getTotalAmount()
            listener.onBuyClicked(amount, viewModel.getShoppingCartItemCount())

            viewModel.isItem1InCart = false
            viewModel.isItem2InCart = false
            viewModel.isItem3InCart = false

            activity?.supportFragmentManager?.popBackStack()
        }
    }
}