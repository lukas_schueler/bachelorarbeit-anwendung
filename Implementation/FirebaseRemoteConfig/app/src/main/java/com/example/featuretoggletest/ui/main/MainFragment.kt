package com.example.featuretoggletest.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.featuretoggletest.MainActivity
import com.example.featuretoggletest.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    companion object {
        fun newInstance(homeListener: MainActivity.HomeListener): MainFragment {
            return MainFragment().apply {
                activityHomeListener = homeListener
            }
        }
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding

    private lateinit var activityHomeListener: MainActivity.HomeListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        setupCards()
        setupListeners()
    }

    private fun setupCards() {
        binding.itemCard1.isChecked = viewModel.isItem1InCart
        binding.itemCard2.isChecked = viewModel.isItem2InCart
        binding.itemCard3.isChecked = viewModel.isItem3InCart
    }

    private fun setupListeners() {
        binding.itemCard1.setOnClickListener { onItem1Clicked() }
        binding.itemCard2.setOnClickListener { onItem2Clicked() }
        binding.itemCard3.setOnClickListener { onItem3Clicked() }
        binding.shoppingCartButton.setOnClickListener { onShoppingCartClick() }
    }

    private fun onItem1Clicked() {
        viewModel.isItem1InCart = !viewModel.isItem1InCart
        binding.itemCard1.isChecked = viewModel.isItem1InCart
    }

    private fun onItem2Clicked() {
        viewModel.isItem2InCart = !viewModel.isItem2InCart
        binding.itemCard2.isChecked = viewModel.isItem2InCart
    }

    private fun onItem3Clicked() {
        viewModel.isItem3InCart = !viewModel.isItem3InCart
        binding.itemCard3.isChecked = viewModel.isItem3InCart
    }

    private fun onShoppingCartClick() {
        activityHomeListener.onShoppingCartClicked()
    }
}