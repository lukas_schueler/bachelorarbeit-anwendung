package com.example.featuretoggletest.ui.main

import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    private val item1Cost = 4.12
    private val item2Cost = 3.25
    private val item3Cost = 6.24

    var isItem1InCart = false
    var isItem2InCart = false
    var isItem3InCart = false

    fun getShoppingCartSum(): Double {
        var sum = 0.0

        if(isItem1InCart){
            sum += item1Cost
        }

        if (isItem2InCart){
            sum += item2Cost
        }

        if(isItem3InCart){
            sum += item3Cost
        }

        return  sum
    }

    fun getShoppingCartItemCount(): Int {
        var sum = 0

        if(isItem1InCart){
            sum++
        }

        if (isItem2InCart){
            sum++
        }

        if(isItem3InCart){
            sum++
        }

        return  sum
    }
}