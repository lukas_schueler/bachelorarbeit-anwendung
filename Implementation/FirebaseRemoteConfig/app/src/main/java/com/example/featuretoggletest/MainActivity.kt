package com.example.featuretoggletest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.*
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.featuretoggletest.databinding.MainActivityBinding
import com.example.featuretoggletest.ui.main.MainFragment
import com.example.featuretoggletest.ui.main.MainViewModel
import com.example.featuretoggletest.ui.main.ShoppingCartFragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings

class MainActivity : AppCompatActivity() {

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val remoteConfig = Firebase.remoteConfig
    private var isNewLoadingAnimationEnabled = remoteConfig.getBoolean("new_loading_animation")

    private lateinit var binding: MainActivityBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firebaseAnalytics = Firebase.analytics
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        if (savedInstanceState == null) {
            Thread {
                Thread.sleep(2000)
                this.runOnUiThread {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, MainFragment.newInstance(getHomeListener()))
                        .commitNow()
                }
            }.start()
        }
    }

    override fun onResume() {
        super.onResume()

        if (isNewLoadingAnimationEnabled) {
            showCompleteOverlay()
            startLoadingAnimation()
        } else {
            removeCompleteOverlay()
        }

        configureRemoteConfig()
    }

    private fun configureRemoteConfig() {
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 0
        }
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        remoteConfig.fetchAndActivate()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Log.d("Firebase: ", "Config params updated: $updated")
                    //Toast.makeText(this, "Fetch and activate succeeded", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(
                        this, "Fetch failed",
                        Toast.LENGTH_SHORT
                    ).show()
                    task.exception?.printStackTrace()
                }
            }
    }

    private fun startLoadingAnimation() {
        scaleImageUp { scaleImageDown { scaleImageUp { scaleImageDown { scaleImageUp { endLoadingAnimation() } } } } }
    }

    private fun scaleImageUp(endAction: () -> Unit) {
        binding.loadingImage.animate()
            .setStartDelay(500)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .setDuration(1000)
            .scaleXBy(1.5f)
            .scaleYBy(1.5f)
            .withEndAction(endAction)
            .start()
    }

    private fun scaleImageDown(endAction: () -> Unit) {
        binding.loadingImage.animate()
            .setStartDelay(500)
            .setInterpolator(BounceInterpolator())
            .setDuration(1000)
            .scaleXBy(-1.5f)
            .scaleYBy(-1.5f)
            .withEndAction(endAction)
            .start()
    }

    private fun endLoadingAnimation() {
        removeViewAnimated(binding.loadingImage)
        removeViewAnimated(binding.loadingTextview, 500)
        fadeoutView(binding.overlay, 1000) { removeCompleteOverlay() }
    }

    private fun removeViewAnimated(view: View, startDelay: Long = 0L) {
        view.animate()
            .setInterpolator(AnticipateInterpolator())
            .setStartDelay(startDelay)
            .setDuration(500)
            .scaleX(0f)
            .scaleY(0f)
            .start()
    }

    private fun fadeoutView(view: View, startDelay: Long, endAction: () -> Unit = {}) {
        view.animate()
            .setStartDelay(startDelay)
            .setDuration(500)
            .alpha(0f)
            .withEndAction(endAction)
            .start()
    }

    private fun removeCompleteOverlay() {
        setCompleteOverlayVisibility(View.GONE)
    }

    private fun showCompleteOverlay() {
        setCompleteOverlayVisibility(View.VISIBLE)
        binding.overlay.alpha = 1f
        binding.loadingTextview.scaleX = 1f
        binding.loadingTextview.scaleY = 1f
        binding.loadingImage.scaleX = 1f
        binding.loadingImage.scaleY = 1f

    }

    private fun setCompleteOverlayVisibility(visibility: Int) {
        binding.loadingImage.visibility = visibility
        binding.loadingTextview.visibility = visibility
        binding.overlay.visibility = visibility
    }

    private fun getHomeListener(): HomeListener {
        return object : HomeListener {
            override fun onShoppingCartClicked() {
                supportFragmentManager.beginTransaction()
                    .setCustomAnimations(
                        R.anim.enter_from_right,
                        R.anim.exit_to_left,
                        R.anim.enter_from_left,
                        R.anim.exit_to_right
                    )
                    .replace(
                        R.id.container,
                        ShoppingCartFragment.newInstance(getShoppingCartListener())
                    )
                    .addToBackStack(null)
                    .commit()

                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
                    param(
                        FirebaseAnalytics.Param.ITEMS,
                        viewModel.getShoppingCartItemCount().toString()
                    )
                }
            }
        }
    }

    private fun getShoppingCartListener(): ShoppingCartListener {
        return object : ShoppingCartListener {
            override fun onBuyClicked(price: Double, itemCount: Int) {
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
                    param(FirebaseAnalytics.Param.PRICE, price)
                    param(FirebaseAnalytics.Param.DISCOUNT, remoteConfig.getDouble("discount_10_euro"))
                }
                firebaseAnalytics.logEvent("bought_items") {
                    param("price", price)
                    param("item_count", itemCount.toLong())
                }
                firebaseAnalytics.setUserProperty("last_shopping_cart_sum", price.toString())
            }
        }
    }

    interface HomeListener {
        fun onShoppingCartClicked()
    }

    interface ShoppingCartListener {
        fun onBuyClicked(price: Double, itemCount: Int)
    }
}